Source: libsoundio
Maintainer: Debian Hamradio Maintainers <debian-hams@lists.debian.org>
Uploaders:
 Christoph Berg <myon@debian.org>,
Section: libs
Priority: optional
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 doxygen,
 libasound2-dev,
 libjack-jackd2-dev,
 libpulse-dev,
 pkg-config,
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian-hamradio-team/libsoundio
Vcs-Git: https://salsa.debian.org/debian-hamradio-team/libsoundio.git
Homepage: http://libsound.io/

Package: libsoundio-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends:
 libsoundio2 (= ${binary:Version}),
 ${misc:Depends},
Description: cross platform audio input and output library (development files)
 libsoundio is a lightweight abstraction over various sound drivers. It provides
 a well-documented API that operates consistently regardless of the sound driver
 it connects to. It performs no buffering or processing on your behalf; instead
 exposing the raw power of the underlying backend.
 .
 libsoundio is appropriate for games, music players, digital audio workstations,
 and various utilities.
 .
 libsoundio is serious about robustness. It even handles out of memory
 conditions correctly.
 .
 This package contains the development files.

Package: libsoundio2
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: cross-platform audio input and output library
 libsoundio is a lightweight abstraction over various sound drivers. It provides
 a well-documented API that operates consistently regardless of the sound driver
 it connects to. It performs no buffering or processing on your behalf; instead
 exposing the raw power of the underlying backend.
 .
 libsoundio is appropriate for games, music players, digital audio workstations,
 and various utilities.
 .
 libsoundio is serious about robustness. It even handles out of memory
 conditions correctly.
 .
 This package contains the shared library.
