libsoundio (2.0.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Christoph Berg <myon@debian.org>  Fri, 11 Mar 2022 11:16:01 +0100

libsoundio (2.0.0-1) experimental; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Christoph Berg ]
  * Fix watch file to see github tarballs.
  * New upstream version 2.0.0; bump soname to libsoundio2.
  * DH 13 and mark /usr/bin/sio_* as not-installed.
  * Drop dh_strip --dbgsym-migration code.
  * Add debian/gitlab-ci.yml.
  * Adopt package under the Debian Hamradio Team umbrella. (Closes: #909881)

 -- Christoph Berg <myon@debian.org>  Sun, 25 Apr 2021 23:39:11 +0200

libsoundio (1.1.0-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.

 -- Adrian Bunk <bunk@debian.org>  Wed, 20 Nov 2019 22:04:00 +0200

libsoundio (1.0.2-3) unstable; urgency=medium

  * QA upload.

  [ Jelmer Vernooĳ ]
  * Transition to automatic debug package (from: libsoundio-dbg).

  [ Sebastian Ramacher ]
  * debian/: Bump debhelper compat to 12.
  * debian/control:
    - Bump Standards-Version.
    - Remove obsolete Pre-Depends.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 07 Jul 2019 13:08:55 +0200

libsoundio (1.0.2-2) unstable; urgency=medium

  * QA upload.
  * Orphan package, set maintainer to qa group

 -- Felipe Sateler <fsateler@debian.org>  Sat, 29 Sep 2018 19:20:09 -0300

libsoundio (1.0.2-1) unstable; urgency=medium

  * Update to upstream version 1.0.2.
  * Add arch independent endian detection patch from upstream.
    (Closes: #799335)

 -- Andrew Kelley <superjoe30@gmail.com>  Thu, 24 Sep 2015 09:55:14 -0700

libsoundio (1.0.1-1) unstable; urgency=medium

  * Initial release. (Closes: #798735)

 -- Andrew Kelley <superjoe30@gmail.com>  Fri, 11 Sep 2015 22:29:01 -0700
